// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.


function createNewUser(){
    const userName = prompt ('Введите Имя:');
    const userLastName = prompt ('Введите Фамилию: ');
    const userBirthday = prompt ('Введите дату рождения в формате dd.mm.yyyy');
    const getBirthDayYear = (birthday) => {
        let arr = birthday.split('.');
        let birthdayYear = Number(arr[2]);
        return birthdayYear;
    }

    return {
        firstName: userName,
        lastName: userLastName,
        birthday: userBirthday,
        getLogin() {
           const login = `${this.firstName[0]}${this.lastName}`;
           return login.toLowerCase();      
        },
        getAge() {
            let now = new Date();
            let currentYear = now.getFullYear();
            let birthdayYear = getBirthDayYear(this.birthday);
            return currentYear - birthdayYear;
        },
        getPassword(){
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${getBirthDayYear(this.birthday)}`;
        }
    }
}

const user = createNewUser(); 
console.log(user);    
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
    
// Считать с помощью модального окна браузера два числа.
// Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
// Вывести в консоль результат выполнения функции.
// Необязательное задание продвинутой сложности:
// После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).


function isValidNumber(value) {
    if (value === "" || value === null || Number.isNaN(+value)) {
        return false;
    }
   return true;
}

function mathOperation() {
    let userNumber1 = +prompt('Введите первое число:');
     while (!isValidNumber(userNumber1)){
     userNumber1 = +prompt('Ещё раз введите первое число:');
    }
    let userNumber2 = +prompt('Введите второе число:');
    while (!isValidNumber(userNumber2)){
        userNumber2 = +prompt('Ещё раз введите второе число:');
    }
    const userMathOperation = prompt('Введите знак математической операции: +, -, *, /');
    switch (userMathOperation) {
        case '+':
            return userNumber1 + userNumber2;        
        case '-':
            return userNumber1 - userNumber2;        
        case '*':
            return userNumber1 * userNumber2;        
        case '/':
            return userNumber1 / userNumber2;        
        default:
            break;
    }
}

 console.log(mathOperation());



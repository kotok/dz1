// Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
// Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
// Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя. Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
// Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.

let userName = prompt ('Введите Ваше имя');
while (!userName){
    userName = prompt ('Введите ваше Имя ещё раз');
}

let userAge = prompt('Введите Ваш возраст');
while (Number.isNaN(+userAge) || userAge === null || userAge === ""){
    userAge = prompt('Введите Ваш возраст ещё раз');
}

if (userAge < 18){
    alert('You are not allowed to visit this website.');
} else if (userAge >= 18 && userAge <= 22){
    let doYouSure = confirm ('Are you sure you want to continue?');
    doYouSure ? alert (`Welcome, ${userName}`) : alert ('You are not allowed to visit this website.')
}else {
    alert (`Welcome, ${userName}`);
}
